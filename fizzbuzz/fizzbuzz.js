/*Sample code to read in test cases: */
var fs  = require("fs");
fs.readFileSync(process.argv[2]).toString().split('\n').forEach(function (line) {
    if (line !== "") {
    	var lineArgs = line.split(" ");

    	if(lineArgs.length === 3) {
    		var x = parseInt(lineArgs[0],10);
    		var y = parseInt(lineArgs[1],10);
    		var n = parseInt(lineArgs[2],10);

    		var answers = [];
    		for(var i=1; i <= n; i++) {
    			var divByX = (i%x === 0);
    			var divByY = (i%y === 0);
 
    			if(divByX && divByY) {
    				answers.push("FB");
    			} else if(divByX) {
    				answers.push("F");
    			} else if(divByY) {
    				answers.push("B");
    			} else {
					answers.push(i);
    			}
    		}
    		var ansStr = String(answers);
    		ansStr = ansStr.replace(/,/g," ");
    		console.log(ansStr);
    	} else {
    		console.log("Incorrect number of args");
    	}
    }
});